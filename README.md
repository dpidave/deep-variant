# Running deepvariant for SNP calling using the default model  

## Requires  
The easiest way is to use the docker container.  

### Install docker (below should work for Ubuntu flavour distributions)  
```
BIN_VERSION="0.9.0"

sudo apt -y update
sudo apt-get -y install docker.io
sudo docker pull google/deepvariant:"${BIN_VERSION}"
```

For further details see (https://github.com/google/deepvariant/blob/r0.9/docs/deepvariant-quick-start.md). This includes instructions for downloading some test data that takes about 1 minute to run on my workstation.   

### Genomes resources  
A Reference genome `fasta` file and corresponding `fai` file. If you use the `run_deepvar.sh` script this needs to be in `[CWD]/data/Ref/`  

A sorted `bam` short read alignment file(s) and its corresponding `bai` file (samtools index).  The alignment does not need to be de-replicated, but it probably doesn't hurt. If you want to use this script the `bam` files need to be in `[CWD]/data/BWA_BAM/`.

BTW [CWD] just means your current working directory.  

## The script  
The program runs in a docker container, which is nice. 

Modify line 15 to list your IDs of your target BAMs such that that they follow the pattern `sampleID1 sampleID2 sampleID3`, with these sample names match the sample names for your bam files ie `sampleID1.bam sampleID2.bam sampleID3.bam` that are in `data/BWA_BAMS/` (obviously your filenames will be different).  

The script will probably need to be run with sudo, you can use the following to output StdErr and StdOut to log files (I love bash!).  

```
sudo ./run_deepvar.sh > >(tee -a stdout.log) 2> >(tee -a stderr.log >&2)
```  
