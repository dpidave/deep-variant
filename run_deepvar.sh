#!/bin/bash

## pipe stderr and stdout to files using:
#sudo ./run_deepvar.sh > >(tee -a stdout.log) 2> >(tee -a stderr.log >&2)
##

## this is based on your install version
BIN_VERSION="0.9.0"

## Change the following paths based on your folder structure  
INPUT_DIR="${PWD}/data"
OUTPUT_DIR="${PWD}/output"
REF="REFERENCE_FILENAME.fasta"

mkdir -p "${OUTPUT_DIR}"

for BAM in sampleID1 sampleID2 sampleID3;
do
  echo "Processing ${BAM} started " $(date)
  docker run \
    -v "${INPUT_DIR}":"/input" \
    -v "${OUTPUT_DIR}":"/output" \
    google/deepvariant:"${BIN_VERSION}" \
    /opt/deepvariant/bin/run_deepvariant \
    --model_type=WGS \
    --ref=/input/Ref/${REF} \
    --reads=/input/BWA_BAM/${BAM}.bam \
    --output_vcf=/output/${BAM}.vcf.gz \
    --output_gvcf=/output/${BAM}.bam.g.vcf.gz \
    --num_shards=10 # change to number of threads available
  echo "Processing ${BAM} ended " $(date)
done
